#Importation of Libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#Initialization of global variable
data1=0
data2=0
value=0
NamesOfVariableX=[]
VariableXValues=[]
NamesOfVariableY=[]
VariableYValues=[]



def ConstructVariableY():
        np.set_printoptions(threshold=np.inf)
        np.set_printoptions(suppress=True)
        np.seterr(divide='ignore', invalid='ignore')

        global NamesOfVariableY
        global VariableYValues
        i = -1
        dat = pd.read_excel("C:/Downloads/Data_Sample (3).xlsx", sheetname=1).as_matrix()

        for dt1 in dat:
            i = i + 1
            j = 1
            for ct in range(4, 120):
                Value = dat[i][j]
                j = j + 1
                #print(str(i) + "," + str(j) + "===", Value)
                variableY = dt1[0] + ",PATIENCE" + str(ct)
                NamesOfVariableY.append(variableY)
                VariableYValues.append(Value)
        #print(NamesOfVariableY)
        #print(VariableYValues)


def ConstructVariableX():
     np.set_printoptions(threshold=np.inf)
     np.set_printoptions(suppress=True)
     np.seterr(divide='ignore', invalid='ignore')

     global NamesOfVariableX
     global VariableXValues
     i=-1
     dat=pd.read_excel("C:/Downloads/Data_Sample (3).xlsx",sheetname=0).as_matrix()

     for dt1 in dat:
        i=i+1
        j=1
        for ct in range(4, 120):
          Value = dat[i][j]
          j=j+1
          #print(str(i)+","+str(j)+"===",Value)
          variableX = dt1[0]+",PATIENCE"+str(ct)
          NamesOfVariableX.append(variableX)
          VariableXValues.append(Value)
     #print(NamesOfVariableX)
     #print(VariableXValues)

def PrintVariableXValues():
    ConstructVariableX()
    print("VariableXHeaders : ",NamesOfVariableX)
    print("VariableXValues : ", VariableXValues)

def PrintVariableYValues():
        ConstructVariableY()
        print("VariableYHeaders : ", NamesOfVariableY)
        print("VariableYValues : ", VariableYValues)

def FindCorrelationBetweenXandY():
    np.set_printoptions(threshold=np.inf)
    np.set_printoptions(suppress=True)
    np.seterr(divide='ignore', invalid='ignore')

    global value
    ConstructVariableX()
    ConstructVariableY()
    correlationCoefficient = np.corrcoef(VariableXValues,VariableYValues)
    print("The Correlation Coefficient of X,Y",correlationCoefficient)

def FindCorrelationAndPValueOfXAndY():
    import numpy as np
    from scipy.stats import pearsonr
    ConstructVariableX()
    ConstructVariableY()
    r, p = pearsonr(VariableXValues,VariableYValues)
    print("", r)
    print("p", p)

def GetDataVisualizationAndPattern():
    ConstructVariableX()
    ConstructVariableY()
    X=VariableXValues
    Y=VariableYValues
    a,b=GetLineOfBestFit(X,Y)
    YFit = [a+b*xi for xi in X]
    plt.plot(X,YFit)

    colors=('#ff27CA')
    area = np.pi*3
    plt.scatter(X,Y,s=area,c=colors,alpha=0.5)
    plt.xlim(0.001,200)
    plt.ylim(0.001,200)
    plt.xlabel('HUMAN')
    plt.ylabel('PAF')
    plt.show()

def GetLineOfBestFit(X,Y):
    xBar = sum(X)/len(X)
    yBar = sum(Y)/len(Y)
    n=len(X) # len(Y)
    numerator= sum([xi*yi for xi,yi in zip(X,Y)])-n * xBar*yBar
    denominator = sum([xi**2 for xi in X])-n*xBar**2
    b= numerator/denominator
    a= yBar-b*xBar
    print(a,b)
    return a,b

def test():
    from scipy.stats.stats import pearsonr
    global value
    ConstructVariableX()
    ConstructVariableY()
    r, p = pearsonr(VariableXValues,VariableYValues)
    print("r", r)
    print("p", p)


#FindCorrelationAndPValueOfXAndY()
#GetLineOfBestFit()
GetDataVisualizationAndPattern()
#PrintVariableYValues()
#FindCorrelationBetweenXandY()


